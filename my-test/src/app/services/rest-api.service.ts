import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Departamento } from '../models/departamento';
import { Ciudad } from '../models/ciudad';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  
  apiUrl: String = environment.apiUrl;
  
  constructor(public http: HttpClient) {
  }
    
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }  

    getDeptos() {
      return new Promise(resolve => {
        this.http.get(this.apiUrl+'/deptos').subscribe(data => {
        resolve(data);}, 
        err => {
          console.log(err);
        });
      });
    }

    getCiudades() {
      return new Promise(resolve => {
        this.http.get(this.apiUrl+'/ciudades').subscribe(data => {
        resolve(data);}, 
        err => {
          console.log(err);
        });
      });
    }

}
