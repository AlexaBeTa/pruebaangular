import { Departamento } from './departamento';

export class Ciudad {
    id: string;
    nombre: string;
    Departamento: Departamento;
}
