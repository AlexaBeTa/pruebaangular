import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeRoutingModule } from './home-routing.module';
import { DescripcionComponent } from './components/descripcion/descripcion.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormularioComponent } from './components/formulario/formulario.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HomeRoutingModule,
    FormsModule,
  ],
  declarations: [DescripcionComponent, FooterComponent, FormularioComponent],
  exports: [
    DescripcionComponent,
    FooterComponent,
    FormularioComponent
  ]
})
export class HomeModule { }
