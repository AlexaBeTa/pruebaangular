import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { RestApiService } from '../../../../services/rest-api.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  constructor(public restApi: RestApiService, private fb: FormBuilder) { }

  usuarioForm: FormGroup;
  departamentos: any = [];
  ciudades: any = [];
  ciudadesFiltros: any = [];
  idDepartamento: number;

  ngOnInit() {
    this.listCiudades();
    this.listDeptos();
    this.constructUsuarioForm();
  }

  
  constructUsuarioForm() {
    this.usuarioForm = this.fb.group({
      nombre : ['',[Validators.required]],
      contacto : ['',[Validators.required]],
      email : ['', [Validators.required, Validators.minLength(3), Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]],
      terminos : ['', [Validators.required]],
      politica : ['', [Validators.required]],
      fecha: ['',[Validators.required]],
      departamento: ['',[Validators.required]],
      ciudad: ['',[Validators.required]]
    });
  }


  listCiudades() {
    this.restApi.getCiudades()
    .then(data => {
      this.ciudades = data;
    });
  }

  listDeptos() {
    this.restApi.getDeptos()
    .then(data => {
      this.departamentos = data;
    });
  }

  obtenerCiudad(){
    this.idDepartamento = this.usuarioForm.get('departamento').value;
    if(this.idDepartamento > 0 ){
      this.ciudadesFiltros = this.ciudades.filter( el => el.IdDepto  == this.idDepartamento );
    }else {
      this.ciudadesFiltros = this.ciudades;
    }
  }

}
