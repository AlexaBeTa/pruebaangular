import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  nombreLogos = ["logo1.png", "logo2.png","logo3.png", "logo4.png", "logo5.png","logo6.png"];

  constructor() { }

  ngOnInit() {
  }

}
