# PruebaAngular
Debe estar instalado e node.js y npm package manager, abrir un CMD en la carpeta root donde esta la aplicación de angular y 
escribir npm install, esto instalara todos los paquetes necesarios para correr la aplicación, tales como 
(jsonserver, propper.js, boostrap, jquery, entre otros).
Luego se debe correr la aplicacion angular con el comando:
ng serve -o 

y el servidor json con:
npm run json-server
(cada comando en un CMD distinto al mismo tiempo, para que se muestre las listas desplegables con los datos).
